::
:: This script adds universis-signer controller in windows startup applications.
::
:: get current script directory
@echo off
::SET script_path=%~dp0
:: get installation directory
for %%i in ("%~dp0..\..") do set "install_dir=%%~fi"
:: copy default service configuration properties
echo Copying universis controller properties
xcopy /y /f %install_dir%\extras\windows\service.properties %install_dir%\extras\
:: copy executable wrapper
echo Copying universis controller executable
xcopy /y /f /d %install_dir%\extras\windows\universis-signer64.exe %install_dir%\
:: add service controller to registry
echo Updating windows startup applications
reg add HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Run /v UniversisSigner /t REG_SZ /d "%install_dir%\universis-signer64.exe" /f
echo The operation was completed successfully.
echo Universis signer controller has been registered as startup application and it will be run at windows startup.
pause
