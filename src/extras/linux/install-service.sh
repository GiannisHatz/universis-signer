#!/bin/sh
# This script installs universis-signer as system service
# Important note: Before running this script move it script to installation root directory

# get script path
SCRIPT=$(readlink -f "$0")

# format script path for sed
SCRIPT_PATH=$(echo $(dirname "$SCRIPT") | sed 's_/_\\/_g')

STR="s/SERVICE_PATH/$SCRIPT_PATH/g"
# copy service
sed $STR ./extras/linux/universis-signer.service > /etc/systemd/system/universis-signer.service

chmod 644 /etc/systemd/system/universis-signer.service
chown root:root /etc/systemd/system/universis-signer.service

systemctl enable universis-signer.service

# create symlink for eToken lib
[ -f /usr/lib64/libeToken.so ] && ln -s /usr/lib64/libeToken.so /usr/lib/libeToken.so

systemctl start universis-signer.service
