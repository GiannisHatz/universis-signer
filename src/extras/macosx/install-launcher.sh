INSTALL_DIR=$(cd "$(dirname "$0")"; pwd -P)
# remove previous launcher
[ -f ~/Library/LaunchAgents/universis.signer.plist ] && launchctl unload ~/Library/LaunchAgents/universis.signer.plist
# format installation path
SCRIPT_PATH=$(echo $INSTALL_DIR | sed 's_/_\\/_g')
STR="s/INSTALL_DIR/$SCRIPT_PATH/g"
# copy launcher
sed $STR ./extras/macosx/universis.signer.plist > ~/Library/LaunchAgents/universis.signer.plist
# load launcher
launchctl load -w ~/Library/LaunchAgents/universis.signer.plist
# exit
read -rsn1 -p"Universis signer launcher installation was completed. Press any key to continue...";echo