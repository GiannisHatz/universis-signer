package org.universis.signer;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "routes")
public class SignerRouteConfiguration {

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "route")
    public final List<SignerRoute> routes = new ArrayList<SignerRoute>();



}
