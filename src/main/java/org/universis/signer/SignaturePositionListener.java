package org.universis.signer;

import com.itextpdf.text.pdf.parser.ImageRenderInfo;
import com.itextpdf.text.pdf.parser.LineSegment;
import com.itextpdf.text.pdf.parser.RenderListener;
import com.itextpdf.text.pdf.parser.TextRenderInfo;
import com.itextpdf.text.Rectangle;

public class SignaturePositionListener implements RenderListener {

    public SignatureBlockConfiguration configuration = new SignatureBlockConfiguration();

    public SignaturePositionListener() { }

    public SignaturePositionListener(SignatureBlockConfiguration options) { this.configuration = options; }


    @Override
    public void beginTextBlock() {}

    @Override
    public void endTextBlock() {}

    @Override
    public void renderImage(ImageRenderInfo renderInfo) {}

    public Rectangle position;

    @Override
    public void renderText(TextRenderInfo renderInfo) {
        // Check if this is the text marker
        String text = renderInfo.getText();
        if (this.configuration.signatureBlockText.contains(text)) {
            // get text descent line
            LineSegment ls;
            if (this.configuration.signatureBlockTextPosition.contains("top")) {
                ls = renderInfo.getDescentLine();
            } else {
                ls = renderInfo.getAscentLine();
            }
            // get text bounds
            int x = (int)ls.getBoundingRectange().getX();
            int y = (int)ls.getBoundingRectange().getY();
            int textWidth = (int)ls.getBoundingRectange().width;
            int textHeight = (int)renderInfo.getAscentLine().getBoundingRectange().y -
                    (int)renderInfo.getDescentLine().getBoundingRectange().y;
            int x1;
            int y1;
            // set a default padding
            int paddingX = 0;
            int paddingY = 5;
            // get signature width
            int signatureWidth = this.configuration.signatureBlockWidth;
            // get signature height by removing text height and y-axis padding
            int signatureHeight = this.configuration.signatureBlockHeight - textHeight - paddingY;
            // calculate x when position is set to center (top or bottom)
            int xCenter = x - ((signatureWidth - textWidth) / 2) + paddingX;
            // calculate x when position is set to right (top or bottom)
            int xRight = x - (signatureWidth - textWidth) + paddingX;
            switch (this.configuration.signatureBlockTextPosition) {
                case SignatureBlockTextPosition.CenterTop:
                    x1 = xCenter;
                    y1 = y + paddingY;
                    position = new Rectangle(x1, y1, x1 + signatureWidth, y1 - signatureHeight);
                    break;
                case SignatureBlockTextPosition.RightTop:
                    x1 = xRight;
                    y1 = y + paddingY;
                    position = new Rectangle(x1, y1, x1 + signatureWidth, y1 - signatureHeight);
                    break;
                case SignatureBlockTextPosition.LeftBottom:
                    position = new Rectangle(x, y, x + signatureWidth, y + signatureHeight);
                    break;
                case SignatureBlockTextPosition.CenterBottom:
                    x1 = xCenter;
                    position = new Rectangle(x1, y, x1 + signatureWidth, y + signatureHeight);
                    break;
                case SignatureBlockTextPosition.RightBottom:
                    x1 = xRight;
                    position = new Rectangle(x1, y, x1 + signatureWidth, y + signatureHeight);
                    break;
                default:
                    // SignaturePositionStyle.LeftTop:
                    x1 = x;
                    y1 = y + paddingY;
                    position = new Rectangle(x1, y1, x1 + signatureWidth, y1 - signatureHeight);
                    break;
            }

        }
    }
}