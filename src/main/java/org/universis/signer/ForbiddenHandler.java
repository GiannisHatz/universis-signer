package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;

/**
 * Handles 403 Forbidden error
 */
public class ForbiddenHandler extends ErrorHandler {
    public ForbiddenHandler() {
        super(NanoHTTPD.Response.Status.FORBIDDEN);
    }
}
