package org.universis.signer;

import com.sun.media.sound.InvalidDataException;

public class InvalidPositionException extends InvalidDataException {
    public InvalidPositionException() {
        super("Invalid signature position format.");
    }
}
