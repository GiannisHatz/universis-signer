package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

/**
 * Universis signer application
 */
public class SignerApp extends RouterNanoHTTPD {

    private static final Logger log = LogManager.getLogger(SignerApp.class);
    private static final String DEFAULT_HOST = "localhost";
    private static final int DEFAULT_PORT = 2465;

    protected SignerAppConfiguration configuration;



    public SignerApp() throws IOException {
        super(SignerApp.DEFAULT_HOST, SignerApp.DEFAULT_PORT);
        this.addMappings();
    }

    public SignerApp(SignerAppConfiguration configuration) throws IOException {
        super(SignerApp.DEFAULT_HOST, SignerApp.DEFAULT_PORT);
        this.configuration = configuration;
        this.addMappings();
    }

    @Override
    public void start() throws IOException {
        log.debug("starting({}, {})", NanoHTTPD.SOCKET_READ_TIMEOUT, this.configuration.daemon);
        this.start(NanoHTTPD.SOCKET_READ_TIMEOUT, this.configuration.daemon);
        log.debug("started({}, {})", NanoHTTPD.SOCKET_READ_TIMEOUT, this.configuration.daemon);
    }

    @Override
    public void addMappings() {
        NanoHTTPD.mimeTypes().put("json", "application/json");
        super.addMappings();
        // get additional routes

        if (this.configuration.routeConfiguration != null) {
            log.debug("Enumerating routes");
            if (this.configuration.routeConfiguration.routes.size() == 0) {
                log.info("Configurable routes are empty. Continue.");
            }
            this.configuration.routeConfiguration.routes.forEach(signerRoute -> {
                try {

                    if (signerRoute.plugin != null) {
                        log.debug("addRoute({},{},{})", signerRoute.plugin, signerRoute.path, signerRoute.className);
                        addRoute(signerRoute.path, SignerRoute.loadClass(signerRoute.plugin, signerRoute.className), this.configuration);
                    } else {
                        log.debug("addRoute({},{})", signerRoute.path, signerRoute.className);
                        addRoute(signerRoute.path, Class.forName(signerRoute.className), this.configuration);
                    }


                } catch (ClassNotFoundException | MalformedURLException e) {
                    log.error("addRoute({},{})", signerRoute.path, signerRoute.className);
                    log.error(ExceptionUtils.getStackTrace(e));
                }
            });
        } else {
            log.info("Configurable routes are undefined. Continue.");
        }
        addRoute("/", RootHandler.class);
        addRoute("/index.html", RootHandler.class);
        addRoute("/openapi/schema.json", OpenapiHandler.class);
        addRoute("/keystore/certs", KeyStoreHandler.class, this.configuration);
        addRoute("/signature/inspect", InspectSignatureBlockHandler.class, this.configuration);
        addRoute("/sign/", DocumentSignerHandler.class, this.configuration);
        addRoute("/verify/", VerifyHandler.class, this.configuration);
        addRoute("/slots/", SlotHandler.class, this.configuration);

    }

}
