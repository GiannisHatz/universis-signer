package org.universis.signer;

import com.itextpdf.text.Rectangle;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response;
import fi.iki.elonen.router.RouterNanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD.UriResponder;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.BadPaddingException;
import javax.security.auth.login.FailedLoginException;
import java.io.File;
import java.io.IOException;
import java.security.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Handles /sign requests for signing pdf documents
 */
public class SignerHandler implements UriResponder {
    private static final Logger log = LogManager.getLogger(SignerHandler.class);
    public HashMap<String,String> files = null;

    public SignerHandler() { }

    public SignerHandler(HashMap<String,String> files) {
        this.files = files;
    }

    public Response get(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        // method not allowed
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    public Response put(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        // method not allowed
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    public Response post(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        // get authorization header
        String[] usernamePassword;
        String authorizationHeader = ihttpSession.getHeaders().get("authorization");
        if (authorizationHeader != null && authorizationHeader.startsWith("Basic ")) {
            byte[] decodedBytes = Base64.decodeBase64(authorizationHeader.replaceFirst("Basic ", ""));
            usernamePassword = new String(decodedBytes).split(":");
        } else {
            return new ForbiddenHandler().get(uriResource, map, ihttpSession);
        }
        KeyStore ks = null;
        SignerAppConfiguration configuration = null;

        try {
            if (this.files == null) {
                // parse files
                this.files = new HashMap<>();
                ihttpSession.parseBody(this.files);
                for(String ignored : this.files.values()) {
                    // read
                }
            }
            Map<String, List<String>> params = ihttpSession.getParameters();
            // get certificate thumbprint
            if (!params.containsKey("thumbprint")) {
                return new BadRequestHandler("Parameter thumbprint may not be null.").get(uriResource, map, ihttpSession);
            }
            String thumbprint = params.get("thumbprint").get(0);
            // get pdf file
            String file = null;
            for (String key: this.files.keySet()) {
                if (key.startsWith("file")) {
                    file = this.files.get(key);
                    break;
                }
            }
            if (file == null) {
                return new BadRequestHandler("Parameter file may not be null.").get(uriResource, map, ihttpSession);
            }
            // sign document
            String outFile = File.createTempFile("signed", ".pdf").getAbsolutePath();
            // get configuration
            configuration = uriResource.initParameter(SignerAppConfiguration.class);
            if (configuration == null) {
                return new ServerErrorHandler("Application configuration cannot be empty at this context").get(uriResource, map, ihttpSession);
            }
            if (configuration.keyStore == null) {
                return new ServerErrorHandler("Invalid application configuration. Keystore cannot be empty at this context").get(uriResource, map, ihttpSession);
            }
            // set position
            Rectangle position = null;
            if (params.containsKey("position")) {
                String requestPosition = params.get("position").get(0);
                String[] dimensions = requestPosition.split(",");
                if (dimensions.length != 4) {
                    return new BadRequestHandler("Invalid signature position format.").get(uriResource, map, ihttpSession);
                }
                for (int i = 0; i < dimensions.length; i++) {
                    dimensions[i] = dimensions[i].trim();
                }
                position = new Rectangle(Integer.parseInt(dimensions[0]), Integer.parseInt(dimensions[1]), Integer.parseInt(dimensions[2]), Integer.parseInt(dimensions[3]));
            }
            String reason = null;
            if (params.containsKey("reason")) {
                reason = params.get("reason").get(0);
            }
            String name = "sig";
            if (params.containsKey("name")) {
                name = params.get("name").get(0);
            }
            String timestampServer = null;
            if (params.containsKey("timestampServer")) {
                timestampServer = params.get("timestampServer").get(0);
            }
            int page = 1;
            if (params.containsKey("page")) {
                page = Integer.parseInt(params.get("page").get(0));
            }
            // try to get key store
            try {
                ks = configuration.getKeyStore(usernamePassword[1]);
            }
            catch (KeyStoreException e) {
                log.error(ExceptionUtils.getStackTrace(e));
                Throwable cause = e.getCause();
                if (cause instanceof NoSuchAlgorithmException) {
                    return new NotFoundHandler(e).get(uriResource, map, ihttpSession);
                }
                return new ServerErrorHandler(e).get(uriResource, map, ihttpSession);
            }
            catch (IOException e) {
                log.error(ExceptionUtils.getStackTrace(e));
                Throwable cause = e.getCause();
                if (cause instanceof BadPaddingException || cause instanceof FailedLoginException || cause instanceof UnrecoverableKeyException) {
                    return new UnauthorizedHandler(e).get(uriResource, map, ihttpSession);
                }
                return new ServerErrorHandler(e).get(uriResource, map, ihttpSession);
            }
            // get image file
            String imageFile = null;
            for (String key: this.files.keySet()) {
                if (key.startsWith("image")) {
                    imageFile = this.files.get(key);
                    break;
                }
            }
            Signer signer = new Signer(ks);
            signer.sign(file, outFile, thumbprint, usernamePassword[1], reason, page, position, timestampServer, name, imageFile);
            // return outFile
            File f = new File(outFile);
            if (!f.exists()) {
                // close key store
                configuration.tryCloseKeyStore(ks);
               return new NotFoundHandler().get(uriResource, map, ihttpSession);
            }
            String contentDisposition = "attachment; filename=\"" + f.getName() + "\"";
            Response res = new FileStreamHandler(outFile, "application/pdf").get(uriResource, map, ihttpSession);
            res.addHeader("Content-Disposition", contentDisposition);
            CorsHandler.enable(res);
            // set client origin if any
            Map<String, String> headers = ihttpSession.getHeaders();
            if (headers.containsKey("origin")) {
                res.addHeader("Access-Control-Allow-Origin", headers.get("origin"));
            }
            // close key store
            configuration.tryCloseKeyStore(ks);
            // and return
            return res;
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            if (configuration != null && ks != null) {
                try {
                    configuration.tryCloseKeyStore(ks);
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
            return new ServerErrorHandler(e).get(uriResource, map, ihttpSession);
        }
    }

    public Response delete(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        // method not allowed
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    public Response other(String s, RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new CorsHandler().other(s, uriResource, map, ihttpSession);
    }
}
