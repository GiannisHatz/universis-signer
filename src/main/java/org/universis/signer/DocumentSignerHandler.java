package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DocumentSignerHandler implements RouterNanoHTTPD.UriResponder {

    private static final Logger log = LogManager.getLogger(SignerHandler.class);
    public HashMap<String,String> files = null;

    @Override
    public NanoHTTPD.Response get(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response put(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response post(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        try {
            // get user
            UsernamePasswordCredentials user = AuthHandler.getUser(ihttpSession);
            if (user == null) {
                return new ForbiddenHandler().get(uriResource, map, ihttpSession);
            }
            // parse body
            this.files = new HashMap<>();
            ihttpSession.parseBody(this.files);
            for(String ignored : this.files.values()) {
                // read
            }
            // try to find file* param
            String fileKey = null;
            for (String key: files.keySet()) {
                if (key.startsWith("file")) {
                    fileKey = key;
                    break;
                }
            }
            if (fileKey == null) {
                return new BadRequestHandler("Parameter file may not be null.").get(uriResource, map, ihttpSession);
            }
            // get form data
            Map<String, List<String>> params = ihttpSession.getParameters();
            // get original file name
            String inFile = params.get(fileKey).get(0);
            // and file type
            String fileType = "." + FilenameUtils.getExtension(inFile).toLowerCase();
            // find handler based on file type
            switch (fileType) {
                case ".docx":
                case ".xlsx":
                    return  new OfficeDocumentSignHandler(this.files).post(uriResource, map, ihttpSession);
                case ".pdf":
                    return  new SignerHandler(this.files).post(uriResource, map, ihttpSession);
                default:
                    return new BadRequestHandler("Unsupported document type.").get(uriResource, map, ihttpSession);
            }

        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
            return new ServerErrorHandler(e).get(uriResource, map, ihttpSession);
        }

    }

    @Override
    public NanoHTTPD.Response delete(RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new MethodNotAllowedHandler().get(uriResource, map, ihttpSession);
    }

    @Override
    public NanoHTTPD.Response other(String s, RouterNanoHTTPD.UriResource uriResource, Map<String, String> map, NanoHTTPD.IHTTPSession ihttpSession) {
        return new CorsHandler().other(s, uriResource, map, ihttpSession);
    }
}
