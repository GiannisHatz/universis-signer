import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.universis.signer.OfficeDocumentSigner;
import org.universis.signer.VerifySignatureResult;
import sun.security.pkcs11.wrapper.PKCS11Exception;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.TransformException;
import javax.xml.crypto.dsig.XMLSignatureException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;

import static org.junit.jupiter.api.Assertions.*;

public class OfficeDocumentSignerTest {

    @org.junit.jupiter.api.Test
    public void shouldLoadDocument() throws IOException, InvalidFormatException {

        String fileName = SignerTest.class.getResource("students.xlsx").getPath();
        OPCPackage xlsx = OPCPackage.open(fileName, PackageAccess.READ_WRITE);
        assertNotNull(xlsx);
        xlsx.close();
    }

    @org.junit.jupiter.api.Test
    public void shouldSignDocument() throws IOException, InvalidFormatException, GeneralSecurityException, XMLSignatureException, MarshalException, TransformException, NoSuchFieldException {

        String inFile = SignerTest.class.getResource("students.xlsx").getPath();
        String outFile = "tmp/students_signed.xlsx";
        // load test keystore
        KeyStore ks = KeyStore.getInstance("pkcs12");
        String keyStorePath = SignerTest.class.getResource("keystore.p12").getPath();
        ks.load(new FileInputStream(keyStorePath), "secret".toCharArray());
        OfficeDocumentSigner signer = new OfficeDocumentSigner(ks);
        signer.sign(new File(inFile), new File(outFile),
                "d33ac6cc6290bcfb4a23243af0ec98b4f49b2238",
                "secret",
                null,
                null);
    }

    public void shouldSignWithPKCS11() throws IOException, InvalidFormatException, GeneralSecurityException, XMLSignatureException, MarshalException, PKCS11Exception, IllegalAccessException, NoSuchFieldException, TransformException {

        String inFile = SignerTest.class.getResource("students.xlsx").getPath();
        String outFile = "tmp/students_signed.xlsx";

        // load test keystore
        String configFilepath = SignerTest.class.getResource("eToken.windows.cfg").getPath();
        Provider p = new sun.security.pkcs11.SunPKCS11(configFilepath);
        Security.addProvider(p);
        KeyStore ks = KeyStore.getInstance("pkcs11");
        ks.load(null, "1234".toCharArray());

        OfficeDocumentSigner signer = new OfficeDocumentSigner(ks);
        signer.sign(new File(inFile), new File(outFile),
                "f641d66d49ba43bf911e873889d7fa0e32e26ce6",
                "1234",
                null,
                null);
    }

    @org.junit.jupiter.api.Test
    public void shouldVerifySignature() throws IOException, InvalidFormatException, GeneralSecurityException, XMLSignatureException, MarshalException, TransformException, NoSuchFieldException {

        String inFile = SignerTest.class.getResource("students.xlsx").getPath();
        String outFile = "tmp/students_signed.xlsx";
        // load test keystore
        KeyStore ks = KeyStore.getInstance("pkcs12");
        String keyStorePath = SignerTest.class.getResource("keystore.p12").getPath();
        ks.load(new FileInputStream(keyStorePath), "secret".toCharArray());
        OfficeDocumentSigner signer = new OfficeDocumentSigner(ks);
        signer.sign(new File(inFile), new File(outFile),
                "d33ac6cc6290bcfb4a23243af0ec98b4f49b2238",
                "secret",
                null,
                null);

        VerifySignatureResult result = signer.verify(new File("tmp/students_signed.xlsx"));
        assertTrue(result.valid);
        inFile = SignerTest.class.getResource("students.xlsx").getPath();
        result = signer.verify(new File(inFile));
        assertFalse(result.valid);

    }

}
